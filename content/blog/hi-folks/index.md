---
title: My new blog
date: '2019-01-22'
tags: ["ssg", "Gatsby"]
---

Trying a new blogging system: Gatsby

## Can use Markdown

This is a *strong* advantage!

For info I used Gatsby Static Site Generator (SSG) with the [gatsby-starter-blog](https://github.com/gatsbyjs/gatsby-starter-blog).

I can write the posts in Markdown.

## Tag, Classification

I will add tags and categories, classification plugins soon.

- Doc: https://www.gatsbyjs.org/docs/adding-tags-and-categories-to-blog-posts/
- Plugins : (first 2 share the same author)
  - https://www.gatsbyjs.org/packages/gatsby-plugin-tags/?=tag
  - https://www.gatsbyjs.org/packages/gatsby-plugin-categories/?=tag
  - https://www.gatsbyjs.org/packages/@mdotasia/gatsby-plugin-tags/?=tag (seems to be complex and specific)

Trying to install the first two give errors. Will investigate if it is an issue with the starter-blog I used or the plugins. At least the plugins are in-line with the documentation. 

The documentation uses `path`, `posts` and the "old" `frontmatter` that the plugin mention having used. 

After a more research, it looks like these plugins uses a `PostListing` that only exist in some repo. For example https://github.com/Vagr9K/gatsby-advanced-starter/blob/master/src/components/PostListing/PostListing.jsx ...

My current plan is to go with the documentation and create something similar to: https://github.com/justinformentin/gatsby-v2-tutorial-starter .

The `path` is just a field that the blog writer can set in the Markdown `frontmatter`.

Seems like `slug` is used instead of `path`. Also, the way they are called is different: looks like ` PageContext` is not available with the method shown in the documentation?

