---
title: My Second Post!
date: '2019-01-22'
---

Another example post.

It should support tagging and categories. 

Really, all the Static Site Generators (SSG) are using NodeJS, GoLang or else ...  If you want to use Java or Kotlin, you are out of luck. 

Ok, there is JBake, but look at https://www.StaticGen.com/ : it is 50 times less popular than the top ones and the number of contributors is very low too.


It is all about [JAMStack](https://jamstack.org/) : _Modern web development architecture based on client-side JavaScript, reusable APIs, and prebuilt Markup._

