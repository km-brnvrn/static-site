---
title: Workflowy complete export
date: '2019-02-01T16:47+01:00'
tags: ["km", "workflowy"]
---

# Workflowy export

[Workflowy](https://workflowy.com/) is a very nice (simple, fast) outline, notes manager.

They lack an official API to interact with, sadly and they are not Open Source.

There are alternatives, but I never found one that match my criteria so well.

Anyway, here is a simple [TamperMonkey](https://tampermonkey.net/) script that will export all your data, using the [Workflowy Extension API](https://workflowy.com/s/workflowy-extension-api/6ziauXitmUj7idN2).


```
// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://workflowy.com/
// @grant        none
// @run-at       document-idle
// ==/UserScript==


(function() {
    'use strict';
    console.log("On tourne!");

    // Your code here...
})();


function dumpChild(it) {
    var childs = it.getChildren();
    var message = "";
    if (childs.length > 0) {
        message = "<ul>"
        childs.forEach( item => {
            var note = item.getNote();
            if (note) {
                note = "\n<details>"+ note +"</details>\n";    // HTML5  https://www.w3schools.com/tags/tag_details.asp
            } else {
                note="";
            }
            message += "<li "
                +"id='"+ item.getId() +"' "
                +"dateModified='"+ item.getLastModifiedDate().toISOString().slice(0,10) +"' "
                +">"+ item.getName()
                + note
                + dumpChild(item)
                + "</li>\n";
        });
        message += "</ul>\n"
    }
    //console.log(message);
    return message;
}


setTimeout(function() {

    var itemToDumpName =  "Projects"; // "Projects" "Knowledge Base" "Notes" "Top Tasks"
    var itemToDumpId;

    WF.rootItem().getChildren().forEach(
        c => {
            console.log(c.getName());
            if ( c.getName() == itemToDumpName ) {
                itemToDumpId = c.getId();
            }
        }
    );

    var itemToDump = WF.getItemById(itemToDumpId);
    var message = "<article><h1>"+ itemToDumpName +"</h1>";
    message += dumpChild(itemToDump) + "</article>";
    console.log("<html><body>\n"+ message+"/n</body></html>");
    //WF.showAlertDialog(message);

}, 3000);    // Workflowy requires time to be ready ...

```


## Next

Seems that there may be even simplier ways to use Workflowy API : https://gist.github.com/rawbytz

## Chrome extensions

At first, I tried to make a Chrome extension first, but it was too complicated to access the Workflowy javascript functions.

There are already many chrome extensions related to Workflowy, but none of them uses the API. They access the DOM and it does not feel very clean (and the code is very complex).  Typically, the modification date is easily accessible with the API, but not in the DOM.


