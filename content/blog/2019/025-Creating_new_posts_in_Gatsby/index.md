---
title: Creating new posts in Gatsby
date: '2019-01-25T10:15+01:00'
tags: ["ssg", "Gatsby", "Bash"]
---

I made a [script](https://gitlab.com/km-brnvrn/static-site/blob/master/new-post.sh) to ease the creation of new posts in Gatsby SSG.

It simply creates the folder and the file with the title and date. 

It is definitively less *instantaneous* as Workflowy, but it works.
Also it does not track the update date currently, only the creation date.

