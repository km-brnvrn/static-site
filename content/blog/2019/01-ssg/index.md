---
title: Static Site Generator
date: '2019-01-22'
tags: ["ssg", "km"]
---

Static Site Generator (SSG) are really a good idea. All the good components seem to align make it:
- GitLab CI allows to run the site generation for each commit
- GitLab repo allows to store and keep an history of all posts
- GitLab allow to host static pages

No doubt the result is faster and cheaper than any other solution.

## GitLab CI

The GitLab CI allows to run anything, basically for free. It is triggered by commits or others actions.
It leverages Docker and you can define your own "machine".

Generating a site with Kotlin is not an issue.

## SSG market

SSG is dominated by NodeJS, GoLang ... with projects at 30.000 stars. The first Java project has about 700 stars. https://www.StaticGen.com/ 

It is really a cultural thing. See [JAMStack](https://jamstack.org/) : _Modern web development architecture based on client-side JavaScript, reusable APIs, and prebuilt Markup._
 

