---
title: CMS Hugo, Orchid and taxonomies
date: '2020-03-21T15:24+01:00'
tags: ["Tags", "CMS"]
---

# Taxonomies support in CMS and Static Sites Generators (SSG)

I just recently looked again at 

- Looks like Hugo (GoLang) has an excellent support for Taxonomies : https://gohugo.io/content-management/taxonomies/
- Orchid (Kotlin) has created the same (maybe a bit more complicated) : https://orchid.run/plugins/orchidtaxonomies
- Gatsby (NodeJS) has some, but seems more messy (maybe due to Gatsby size)  https://www.gatsbyjs.org/packages/@arrempee/gatsby-plugin-taxonomies/

I guess I will try Orchid as it seems to get some tracktion and is an opportunity to learn some Kotlin.


