---
title: DIGImend and kernel update
date: '2020-03-21T13:35+01:00'
tags: ["Kernel", "Linux", "Tablet"]
---

# Kernel update issue with DIGImend drivers

Updating 
- from kernel-5.5.7-200
- to   kernel-5.5.10-200

it succedeed, but with warnings. 

## Output for dnf update

```
Running scriptlet: kernel-core-5.5.10-200.fc31.x86_64
dkms: running auto installation service for kernel 5.5.10-200.fc31.x86_64

Kernel preparation unnecessary for this kernel.  Skipping...

Building module:
cleaning build area....
make -j8 KERNELRELEASE=5.5.10-200.fc31.x86_64 KVERSION=5.5.10-200.fc31.x86_64....(bad exit status: 2)
Error! Bad return status for module build on kernel: 5.5.10-200.fc31.x86_64 (x86_64)
Consult /var/lib/dkms/digimend/9/build/make.log for more information.
 Done. 
Failed to add dependency on unit, unit systemd-ask-password-plymouth.service does not exist.
dkms: running auto installation service for kernel 5.5.10-200.fc31.x86_64

Kernel preparation unnecessary for this kernel.  Skipping...

Building module:
cleaning build area....
make -j8 KERNELRELEASE=5.5.10-200.fc31.x86_64 KVERSION=5.5.10-200.fc31.x86_64....(bad exit status: 2)
Error! Bad return status for module build on kernel: 5.5.10-200.fc31.x86_64 (x86_64)
Consult /var/lib/dkms/digimend/9/build/make.log for more information.
 Done. 

  Running scriptlet: kernel-core-5.5.7-200.fc31.x86_64
```
...
```
Installed:
  kernel-5.5.10-200.fc31.x86_64           kernel-core-5.5.10-200.fc31.x86_64           kernel-devel-5.5.10-200.fc31.x86_64           kernel-modules-5.5.10-200.fc31.x86_64           kernel-modules-extra-5.5.10-200.fc31.x86_64          

Removed:
  kernel-5.5.7-200.fc31.x86_64            kernel-core-5.5.7-200.fc31.x86_64            kernel-devel-5.5.7-200.fc31.x86_64            kernel-modules-5.5.7-200.fc31.x86_64            kernel-modules-extra-5.5.7-200.fc31.x86_64           

Complete!
```


`cat /var/lib/dkms/digimend/9/build/make.log` :
```
DKMS make.log for digimend-9 for kernel 5.5.10-200.fc31.x86_64 (x86_64)
sam. mars 21 09:12:37 CET 2020
make -C /lib/modules/5.5.10-200.fc31.x86_64/build SUBDIRS=/var/lib/dkms/digimend/9/build modules
make[1]: Entering directory '/usr/src/kernels/5.5.10-200.fc31.x86_64'
  LEX     scripts/kconfig/lexer.lex.c
/bin/sh: flex: command not found
  HOSTCC  scripts/kconfig/parser.tab.o
make[3]: *** [scripts/Makefile.host:9: scripts/kconfig/lexer.lex.c] Error 127
make[3]: *** Waiting for unfinished jobs....
make[2]: *** [Makefile:568: syncconfig] Error 2
make[1]: *** [Makefile:678: include/config/auto.conf.cmd] Error 2
make[1]: Leaving directory '/usr/src/kernels/5.5.10-200.fc31.x86_64'
make: *** [Makefile:22: modules] Error 2
```
Looks like 2 packages are missing:

- flex `flex-2.6.4-3.fc31.x86_64` _I installed OK_
- systemd-ask-password-plymouth _it is not a package missing, maybe a wrong config?_


