#!/bin/sh
#
# Eases the creation of new posts: /content/blog/<year>/<day>-<title>
# Param: title (Special characters will be escaped for folder names)

# Usage: new-post.sh "My \'new\' Post!"

# Debug
set -x

TITLE=${1:-"post-$(date +%H-%M)"}
MPATH=content/blog/$(date +%Y)/$(date +%j)-$(echo -n "$TITLE" | tr -c '[:alnum:]-' '_')

mkdir -p "$MPATH"

# Do not overwrite!
[ -f "$MPATH"/index.md ] && { echo "File already exist!" ; exit 1; }

# Template
tee "$MPATH"/index.md <<EOF
---
title: $TITLE
date: '$(date --iso-8601=minutes)'
tags: ["", ""]
---


EOF

vi "$MPATH"/index.md
