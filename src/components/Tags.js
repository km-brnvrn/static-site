import React from 'react'
import { Link } from 'gatsby'
// Utilities
import kebabCase from "lodash/kebabCase"

// https://www.gatsbyjs.org/docs/css-modules/
import styles from './Tags.module.css'

function Tags(props) {
    const tags = props.tags
    if (tags) {
          return(
              <div class={styles.Tags}><span>Tags:</span>
          <ul>{tags.map( tag => (
          <li key={tag}>
              <Link to={`/tags/${kebabCase(tag)}/`}>
                {tag}
              </Link>
            </li>)
          )}
          </ul></div>)}
    else { 
        return null 
    }
}

export default Tags
