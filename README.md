
This is my Static Site Generator (SSG), using [Gatsby](https://www.gatsbyjs.org/) (Javascript, React, npm ...) 

See the result here: https://km-brnvrn.gitlab.io/static-site/

## Tech
### Gatsby
I kick off my project with the default starter boilerplate. Then I switched to the popular [gatsby-starter-blog](https://github.com/gatsbyjs/gatsby-starter-blog).
I tried to add tags and classifications with plugins, but they are not compatible with the starter blog.
I did them on my own, following the documentation.

### Hosting on GitLab

The pages are hosted on GitLab Pages: https://pages.gitlab.io/gatsby

GitLab provides both:
- Static Pages hosting service 
- Continuous Integration (CI) allowing to rebuild the site on each commit

## TODO

- I still have some styling to do ...
- Look at the Robot.txt for searches
- Display tags on the front page
- ...

## Knowledge Management 

The goal is to have some kind of Knowledge Management system. Hope I can cope with Javascript and React to do it.

